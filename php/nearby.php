<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
</head>
<body>
<?php

  require_once('connectvars.php');

  $lat = $_GET['lat'];
  $lng = $_GET['lng'];
  $name = $_GET['name'];
  $addr = $_GET['addr'];
?>

<table>
<tr><td><strong>Nosaukums:</strong></td><td><?= $name ?></td></tr>
<tr><td><strong>Adrese:</strong></td><td><?= $addr ?></td></tr>
</table>

<h4>Pieturas:</h4>

<ul>

<?php
  
  // Connect to the database and change character set to utf8
  $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($conn, "utf8");

  // use prepared statement when filtering to avoid injection attack
  $stmt = $conn->prepare("
    SELECT stops.stop_name, stops.stop_lat, stops.stop_lon, ROUND(6371000 *
          acos(cos(radians( ? )) * cos(radians(stops.stop_lat)) *
          cos(radians( ? ) - radians(stops.stop_lon)) +
          sin(radians( ? )) * sin(radians(stops.stop_lat))), 0) AS distance
          FROM stops
    HAVING distance < 5000
  ");
  $stmt->bind_param('ddd', $lat, $lng, $lat);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($stop_name, $stop_lat, $stop_lon, $distance);
  $locations = [];
  $descriptions = [];
  $distances = [];
  while ($stmt->fetch()) {
      echo '<li>' . $stop_name . ' '  . $distance .  '</li>';
      array_push($locations, "new Microsoft.Maps.Location($stop_lat, $stop_lon)");
      array_push($descriptions, "'$stop_name'");
      array_push($distances, "'$distance m'");
  }
  $locstring = join(', ', $locations);
  $descstring = join(', ', $descriptions);
  $diststring = join(', ', $distances);
  $stmt->close();
  $conn->close();

?>                                                

</ul>

<div id="myMap" style="position:relative;width:600px;height:400px;"></div>

<script type='text/javascript'>
    var map, infobox;

    function GetMap() {
        var center = new Microsoft.Maps.Location(<?= $lat ?>, <?= $lng ?>),
        map = new Microsoft.Maps.Map('#myMap', {
            center
        });

        //Create an infobox at the center of the map but don't show it.
        infobox = new Microsoft.Maps.Infobox(map.getCenter(), {
            visible: false
        });

        //Assign the infobox to a map instance.
        infobox.setMap(map);


        //Create custom Pushpin
        var pin = new Microsoft.Maps.Pushpin(center, {
            icon: 'images/red_pin.png',
            anchor: new Microsoft.Maps.Point(12, 39)
        });
        map.entities.push(pin);

        //Create stop locations 
        var stopLocations = [<?= $locstring ?>]; //Microsoft.Maps.TestDataGenerator.getLocations(5, map.getBounds());     
        var descriptions = [<?= $descstring ?>]; 
        var distances = [<?= $diststring ?>]; 

        for (var i = 0; i < stopLocations.length; i++) {
            var pin = new Microsoft.Maps.Pushpin(stopLocations[i]);

            //Store some metadata with the pushpin.
            pin.metadata = {
                title: distances[i],
                description: descriptions[i]
            };

            //Add a click event handler to the pushpin.
            Microsoft.Maps.Events.addHandler(pin, 'click', pushpinClicked);

            //Add pushpin to the map.
            map.entities.push(pin);
        }
    }
    function pushpinClicked(e) {
        //Make sure the infobox has metadata to display.
        if (e.target.metadata) {
            //Set the infobox options with the metadata of the pushpin.
            infobox.setOptions({
                location: e.target.getLocation(),
                title: e.target.metadata.title,
                description: e.target.metadata.description,
                visible: true
            });
        }
    }
    </script>
    <script type='text/javascript' src='http://www.bing.com/api/maps/mapcontrol?callback=GetMap&key=AlNkTf5uXxJ8mUrBW1ru6bO5w_R9fQRFnan553bjEG62iJVf6Qyzso8qjYEyZTWy' async defer></script>

</body>
</html>
