<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
</head>
<body>
<ul>

<?php

  require_once('connectvars.php');

  $trip_id = $_GET['trip'];

  // Connect to the database and change character set to utf8
  $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($conn, "utf8");

  // use prepared statement when filtering to avoid injection attack
  $stmt = $conn->prepare("SELECT stop_times.arrival_time, stops.stop_name
     FROM stop_times JOIN stops ON stop_times.stop_id = stops.stop_id
     WHERE stop_times.trip_id = ?
     ORDER BY stop_times.stop_sequence ASC");
  $stmt->bind_param('i', $trip_id);
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($arrival_time, $stop_name);
  while ($stmt->fetch()) {
      echo '<li>' . $arrival_time . ' '  . $stop_name .  '</li>';
  }

  $stmt->close();
  $conn->close();

?>                                                

</ul>
</body>
</html>
