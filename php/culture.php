<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<style>
#objects {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#objects td, #objects th {
  border: 1px solid #ddd;
  padding: 8px;
}

#objects tr:nth-child(even){background-color: #f2f2f2;}

#objects tr:hover {background-color: #ddd;}

#objects th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>

</head>
<body>
<table id='objects'>

<?php

  require_once('connectvars.php');

  // Connect to the database and change character set to utf8
  $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
  mysqli_set_charset($conn, "utf8");

  // use prepared statement when filtering to avoid injection attack
  $stmt = $conn->prepare("SELECT culture_branch, culture_name, culture_address, culture_lat, culture_lng FROM culture ORDER BY culture_branch");
  $stmt->execute();
  $stmt->store_result();
  $stmt->bind_result($culture_branch, $culture_name,$culture_address, $culture_lat, $culture_lng);
  echo "<tr><th>Nozare</th><th>Nosaukums</th><th>Adrese</th><th>Pieturas</th></tr>\n";
  while ($stmt->fetch()) {
      echo "<tr><td>$culture_branch</td><td>$culture_name</td><td>$culture_address</td> <td> <a href='nearby.php?lat=$culture_lat&lng=$culture_lng&name=$culture_name&addr=$culture_address'>Pieturas</a> </td>  </tr>\n";
  }

  $stmt->close();
  $conn->close();

?>                                                

</table>
</body>
</html>

                                                                                
