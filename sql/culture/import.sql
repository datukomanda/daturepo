CREATE OR REPLACE TABLE culture (
  culture_branch VARCHAR(50),
  culture_name VARCHAR(255),
  isil_code VARCHAR(50),
  culture_address VARCHAR(255),
  culture_lat DECIMAL(9,6),
  culture_lng DECIMAL(9,6),
  lks92_x int(8),
  lks92_y int(8)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


LOAD DATA LOCAL INFILE 'ObjektuSaraksts.csv' INTO TABLE culture FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"' IGNORE 1 LINES CHARACTER SET UTF8;
